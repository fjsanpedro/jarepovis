# Init

## Brief

Executing

    repo init -u <manifest_url> -b <manifest_branch>

will create a directory structure like this:

    .repo
    ├── manifests
    │   ├── default.xml
    │   ├── .git -> ../manifests.git
    │   ├── GLOBAL-PREUPLOAD.cfg
    │   ├── fileincluded.xml
    │   └── README.md
    ├── manifests.git
    │   ├── branches
    │   ├── config
    │   ├── description
    │   ├── FETCH_HEAD
    │   ├── HEAD
    │   ├── hooks
    │   ├── index
    │   ├── info
    │   ├── logs
    │   ├── objects
    │   ├── packed-refs
    │   └── refs
    └── manifest.xml -> manifests/default.xml

As you can see manifest.xml is a link to the default manifest and the git folder points to a directory which holds just the git data

## Implementation

Changes when emulating repo are explained in [emulation](emulation.md)

This wrapper requires an additional argument, romname. This change is made for turning build work simpler and having just one folder for builds.

### Repo checking

The script checks if the repo is valid through a regex, currently supported protocols are *file:///*, *git://*, *http://*, *https://* and ssh (both formats *ssh://* and *user@instance* are supported). It checks if the hostname provided is valid, if no connection is available this check will obviously fail thus stopping the script

### Initialization

The folders *\<romname\>/manifests* and *\<romname\>/manifests.git* are created and `GIT_DIR` is set as *$PWD/\<romname\>/manifests.git*. This action will force git to use this specified folder for storing git data

The remote specified in *\<manifest_url\>* is set in the config file

## Downloading and checkout

The remote repository will be fetched, then the script will move to *\<romname\>/manifests* and check out files from the specified branch. Then, if no .git folder is present it will make a link from *.git* to *../manifests.git* and link *manifest.xml* to the default manifest for compatibility