# JarEPOvis

JarEPOvis is a project for an alternative git wrapper for android repos written in python

His aim is replacing goygle wrapper as roms code downloading tool

It's currently in beta

Table of Contents
=================

   * [JarEPOvis](#jarepovis)
      * [Usage](#usage)
         * [Examples](#examples)
      * [Debug mode](#debugmode)
      * [License &amp; Credits](#license--credits)
      * [TODO](#todo)

## Usage

Usage is pretty much the same as google repo manager, with the exception of -r which sets the romname

For any doubt just write

    jarepovis help

### Examples

For downloading a rom source code just type

    jarepovis init -r rom_name -u manifest_site -b manifest_branch

For adding links that makes this repo compatible with google repo wrapper type:

    jarepovis emulate -r romname

They can be removed by typing

    jarepovis unemulate -r romname

For downloading repositories write:

    jarepovis sync -r romname

The script is also able to selfupdate through:

(linking the executable to the git repository is recommended)

    jarepovis update

## Debug mode

You can enable debug mode by typing

    jarepovis [mode] -d

It's useful for printing infos about what commands the script is using

## Documentation

* [init](doc/init.md)
* [sync](doc/sync.md)
* [emulation](doc/emulation.md)

## License & Credits

This code is licensed under the GNU General Public License version 2 or newer [> GNU](https://www.gnu.org/licenses/gpl-2.0.txt)


## TODO

Write proper documentation
